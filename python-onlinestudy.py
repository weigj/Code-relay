# 语言：python
# 主题：小王和网课的故事

# -*- coding: UTF-8 -*-

# 班主任
teacher = '小白'
# 学生
student = '小王'
# 学生家长
father = '老王'

'''
输出
'''
def frame(x):
    print('----{}----\n'.format(x))

class Study:
    '''
    初始化
    '''
    def __init__(self,teacher,student,father):
        self.teacher = teacher
        self.student = student

    '''
    第一天
    '''
    def day1(self):
        print('{}:同学们，这个知识点很重要，请大家牢记！'.format(teacher))
        print('关键时刻掉链子，不带这么欺负人的，{}忍不住渗出了冷汗，额头和脊梁骨直感觉阵阵发凉，老师会不会以为我是早退？我是不是要挂科了？我是不是毕不了业了？我是不是要上街乞讨了……'.format(student))
        print('在客厅里的{}说：“可能是保险丝坏了，我马上修。”'.format(father))
        print('生活还是得继续，{}拿出手机，再次进入课堂。'.format(student))
        print('直播已结束')

    '''
    第二天
    '''
    def day2(self):
        # 全国统一思政大课(大学生懂得)
        print('真开心，没想到有一天老子也能和王源一起上网课')
        print('wait...   到了2020-3-9-15：00')
        print('****，什么土豆服务器，老子要和王源一起上课')
        print('{}使劲按下f5，疯狂的刷新，然后f5键坏了'.format(student))
        print('{}:同学们把刚开始直播的1分钟的相关截图笔记发上来'.format(teacher))

frame('这得从一只蝙蝠说起')
frame('相应号召，停课不停学，网课开始了')

frame('DAY1')
actvie = Study(teacher,student,father)
actvie.day1()

frame('DAY2')
actvie.day2()

#请开始你的表演