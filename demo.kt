/*
*背景:
*  时间：早晨
*  地点：肉摊旁
*  人物：肉摊老板，IT男，乞丐
*  
*
*/

fun main(args:Array<String>){
      val PorkBoss = people.man()//肉摊老板
      val ITman = people.man()//IT男
      val beggar = people.man()//乞丐
      
           //旁白：某天早晨，肉摊老板正在热火朝天地卖肉

      PorkBoss.work(work.cut)
      PorkBoss.work(work.sale)
      PorkBoss.work(work.cut)
      PorkBoss.work(work.sale)
      PorkBoss.work(work.cut)
      PorkBoss.work(work.sale)
      PorkBoss.work(work.cut)
      PorkBoss.work(work.sale)

      PorkBoss.talk("这样下去不行，会累死的，得找个帮手",PorkBoss)//自言自语
      
             //这时一个穿着西装的男人走上舞台
      ITman.work(null)

      ITman.talk("真是太倒霉了，一大早就被辞退了。",ITman)
             //IT男走到正巧看见了肉摊老板的招聘告示，走近摊子
      ITman.talk("你在招聘人手吗？",PorkBoss)

             //老板看了看IT男，觉得他不适合
      PorkBoss.talk("一公斤猪肉60元，5公斤多少钱",ITman)
            //IT男一听欲言又止，他知道有人掉过一次坑……
      var IT_univalent:Int = 60
      var IT_num:Int = 5
      var IT_much:Int = IT_univalent*IT_num
      ITman.talk("哈哈当然是:"+IT_much.toString+"元了",PorkBoss)

            //老板一听心想这人还不傻，准备让他实习但总觉不对劲，这时进来一个乞丐
      beggar.work（work.begging）
      beggar.talk("慢着，如果只有1.5公斤肉呢？",ITman)
            //此时老板明白过来,IT男恍然大悟
      var beg_univalent:Float = 60
      var beg_num:Float = 1.5
      var beg_much:Float = beg_univalent*beg_num
      beggar.talk("总价为"+beg_much.toString+"元"，ITman)

      PorkBoss.("嗯，他说的对，这个要用小数才能保证不亏。年轻人，知道为什么被辞了吧。回家好好想想",ITman)
      PorkBoss.("你呢，给你个洗澡券，去洗个澡换身衣服，回来干活。遇上我算你幸运",beggar)
      beggar.work(work.cut)

      //IT男听后若有所思

      ITman.talk("嗯，那我先走了，爹。"，PorkBoss)
      beggar.("(!*_*)",beggar)

     //The End
}
   
class people(){
  class man(){
       fun work(work:String){
         println(work)
       }
       fun talk(message:String,people:people){
           println(message)
       }

  class woman(){}

}

class work(){//人物工作
   val sale = "销售"
   val write = "写代码"
   val begging = "乞讨"
   val cut= "切肉"
}          