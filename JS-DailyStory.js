// 编程语言: js
// 故事主题: 娱乐幽默
function story(){
    var bedroom = Object()      //一间平平无奇的卧室
    bedroom.people = '布由'     //卧室里有一个人叫布由
    bedroom.people_status = '躺在床上'      //此时他正躺在床上
    bedroom.clock = 7.5     //此时闹钟已经七点半了
    bedroom.clock.laugh     //闹钟响了
    bedroom.people_status = '挣扎着从床上爬起'
    bedroom.people.lookat(bedroom.clock)        //一看闹钟已经七点半了
    bedroom.people.frightened()     //布由吓了一跳
    bedroom.people.think('想到今天是上学的第一天，自己可不能迟到啊！')
    bedroom.people_status = '嘴里叼着早餐的面包'        //他嘴里叼着早餐的面包就往门外冲。
    delete bedroom.people        //离开了卧室

    var school = Object()    // 学校
    school.students = ['张三','李四']        // 张三，李四 已经来到了学校
    school.students.push("布由")        // 此时 布由 也慌慌张张的来到了学校
    school.students['布由'].talk(school.students['张三'],'你好啊，我叫布由！')       // 布由 与 张三 交谈了起来
    school.students['张三'].talk(school.students['布由'],'你好，我叫张三，很高兴认识你！')
    school.class_bell.laugh     // 上课铃响了

    school.students['布由'].status = 'urgent urination'       //这时布由一阵尿急
    school.students['布由'].talk(school.students,'我去趟洗手间')
    school.toilet = Object()
    school.students['布由'].push(school.toilet)               //布由来到了洗手间
    school.students['布由'].xxx()             //布由解决了内急
    school.students['布由'].open('厕所门')       //布由推开厕所门
    school.students.push('王五')       
    school.students['王五'].talk(school.students['布由'],'诶哟')      //没想到一推开门，门就砸到一个叫王五的人的脸上，王五叫了一声
    school.students['王五'].appearance = '穿着一身西装'             //王五穿着一身西装，显得十分庄重
    school.students['布由'].talk(school.students['王五'],'不好意思，刚才没看见你')
    school.students['王五'].manner = '焦虑'             //王五的深情焦虑了起来
    school.students['王五'].talk(school.students['布由'],'少爷，来不及解释了，快点跟我走')
    school.students['布由'].manner = '布由一脸懵逼'
    school.students['布由'].follow(school.students['王五'])             //不由得布由多想，布由跟了上去
    delete school.students['王五','布由']

    var bus = Object()
    bus.people = ['王五','布由']
    bus.people['王五'].talk(bus.people['布由'],'布由啊，有些事必须要跟你说明一下了')     //在公交车上，王五跟布由娓娓道来
    bus.people['布由'].talk(bus.people['王五'],'王五啊，什么事情需要这么严肃')
    // ... 接下来交给你们了
    // console.log(bedroom)
    bus.stop(); // bus 到站了
    delete bus.people['王五','布由'] // 王五，布由下车了

    var store = {};
    store.people = ['lucy', '杰西卡', '美女'];
    store.people.push("布由"); // 布由来到了商场
    store.people["布由"].look("看到了美女");
    store.people["布由"].talk("哇哦~看到了美女")
// ... 接下来交给你们了
    // console.log(store)
}