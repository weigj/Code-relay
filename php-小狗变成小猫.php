<?php

abstract class Animal // 这世界上有许多的动物
{
    protected $energy; // 他们充满活力
    
    public function eat($food) // 动物需要吃饭
    {
        $this->energy += $food;
    }
   
    public function sleep($hour) // 动物需要睡觉
    {
        sleep($hour * 3600);
        $this->energy -= $hour / 2;
    }
}

interface Bark // 小狗都会汪汪叫
{
    public function bark();
}

interface Mew // 小猫都会喵喵叫
{
    function mew();
}

class Dog // 这是一只可爱的小狗
extends
    Animal // 小狗是动物
implements
    Bark,
    Mew
{
    public function eat(Bone $bone) // 小狗爱吃骨头
    {
        parent::eat($bone);
        bark(); // 吃完要叫汪汪！
    }

    public function bark()
    {
        echo("汪汪！"); // 小狗的叫声：汪汪！
    }

    // 一天它想学习小猫的叫声
    public function Mew()
    {
        echo("汪汪!"); // 不对这是狗叫
        echo("汪!"); // 还是不对
        echo("姆汪！"); // 再加把劲
        echo("瞄嗷！"); // 就差一点了
        echo("喵？"); // 好极了
        echo("喵呜～"); // 小狗学会了猫叫，它现在也是一只猫了！
    }
}
