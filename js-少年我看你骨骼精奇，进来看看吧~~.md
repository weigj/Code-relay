 ```js
 //编程语言：ECMAScript
 //故事主题：这世界上有很多你不知道的武功。
(async function() {
  class 人 {
    constructor(名) {
      this.名 = 名;
      this.说 = (话, ...其他话) => console.log(`${名}: ${话}`, ...其他话);
    }
  }
  let 小明 = new 人("小明");
  let 意大利米其林门口扫地的秃子 = new 人("小凸");
  意大利米其林门口扫地的秃子.说(
    "少年，我看你骨骼精奇，是万中无一的武学奇才，维护世界和平就靠你了，我这有本秘籍-《如来神掌》，见与你有缘，就十块卖给你了！"
  );
  小明.说("我不信，有本事你给我表演一个！");
  意大利米其林门口扫地的秃子.说(
    "先给你表演一个降龙十八掌中最厉害的一招，狗不是狗！ ((狗===狗) === false)"
  );
  小明.说("来！");
  const 狗 = NaN;
  意大利米其林门口扫地的秃子.说(
    "巴啦啦能量无卡拉卡，变！",
    `
    OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOO\`...,OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OO^.......\\OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OO^.........OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OO^..........,OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO/*,OOOOOOOOOOOOOOOOOOOO
OO.............,[OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO...*OOOOOOOOOOOOOOOOOOOO
OO..............  ..*OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO^...=OOOOOOOOOOOOOOOOOOOO
OO^..............  . .OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO/\`.....=OOOOOOOOOOOOOOOOOOOO
OO^.........**...    =OOOOOOOOOOOOOOOOOOOOOOOOOOOo[[[[OOOOOOOOOOOOOOO/. ......OOOOOOOOOOOOOOOOOOOOO
OO^.    ......*.. .../OOO/oOOOOOOOOOOOOOOOOOOOOOOO^.....\\OOOOOOOOOOOOO.....  .OOOOOOOOOOOOOOOOOOOOO
OOO\`    ............=O/\`*/OOOOOOOOOOOOOOOOOOOOOOOOo.    ..[OOOOOOOOOOO^...  .OOOOOOOOOOOOOOOOOOOOOO
OOOO.  ...........*]o\`.]/OOOOOOOOOOOOOOOOOOo/[[[\\[..        .[[OOOOOOOOo....=OOOOOOOOOOOOOOOOOOOOOO
OOOO^   ......\\*.]OO^,\\OOOOOOOOOOOOOOOOOOo\`.      .             .\\OOOOOO]..,OOOOOOOOOOOOOOOOOOOOOOO
OOOOO^    ....=OOOO\`./OOOOOOOOOOOOOOOOOOO/.                       .OOOOOO^*/OOOOOOOOOOOOOOOOOOOOOOO
OOOOO^   ....]/OOO\\/OOOOOOOOOOOOOOOOOOOO/.                          ,OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOO.  ....OOOOOOOOOOOOOOOOOOOOOOOOO\`.                             \\OOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOO\\....,OOOOOOOOOOOOOOOOOOOO[\`*..                                ,\\OOOOOOOOOOOOOOOOOOOOOOOoOOOO
OOOOOOO\\../OOOOOOOOOOOOOOOO\`.   .                                .     =OOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOO\`     ..  .      ..       .       .    O\\]]\`..=OOOOOOOOOOOOOOOOOOOOOOoooo
OOOOOOOOOOOOOOOOOOOOOOO/.        ,OOOOO^,\\O\\].    .,*.      ....=O...\\O\\=OOOOOOOOOOOOOOOOOOOOOOoOOO
OOOOOOOOOOOOOOOOOOO\`*...           =\\/[.. ,OOOOO]]/^*..      .[oOOO^ =O^,OOOOOOOOOOOOOOOOOOOOOooOOO
OOOOOOOOOOOOOOOOO\`    .             .,OOOOOOOOOOO/\`    .      ,=OOOOOOO..OOOOOOOOOOOOOOOOOOOOOooOOO
OOOOOOOOOOOOOOOO^.   ..               ..,.....,.               .*oOOO^...OOOOOOOOOoooooOOOOOOoooOOO
OOOOOOOOOOOOO/..     .                     .                 ... ....   =OOOOOOOOOooOoooOoooOoooOOO
OOOOOOOOOOOOO.       ..                                           ..   .ooOOOOOOOOOOooooOOOOOOooOOO
OOOOOOOOOOOOO.                                                    ... .o=OOOOOOOOOOOOOOOOOOOOOoOOOO
OOOOOOOOOOOO\`.                                                     ..]/]oOOOOOOOOOOOOOOOOOOOOOoOOOO
OOOOOOOOOOOO..                                                      ..[oOOOOOOOOOOOOooOOOOOOOoOOOOO
OOOOOOOOOOOO*.                                                        .oOOOOOOOOOOOOOoOOOOOOoOOOOOO
OOOOOOOOOOOo..                                                .        .=OOOOOOOOOOOOOoOOOOOOOOOOOO
OOOOOOOOOOO^..              ...                         .]/OOOOO\\]]...   \\OOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOO/...               ......                    OOOOOOOOOOOOOOO\\. .OOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOO^..                  .....                   OOOOOOOOOOOOOOOOOO\\.=OOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOO\\.                   ........     .        .OOOOOOOO\\/oOoOOOOOO^.OOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOO^.                  ... ..[.     .       ,OOOOOOOOOOOOOOOOOOO^.OOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOO\\\`.                 ...  ..*     .       =OOOOOOOOOOOOoOOOO/.=OOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOO\`.                 ........\`   .   .   \\OOOOOOOOOOOOOOO/. /OOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOO\\..                 ,\\. ...[\\]o. ..,,],oOOOOOOOOOOOO\` .=OOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO]..                 ,O]\`..=OOOO\\][OoOOOOOOOOOo/[.  ./OOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOO^.                  .O\\\`..OOOOOOO\`]^,*\`[O[,..   .]OOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOO\\.                 .,oOO^..,OOOOO\`,OOOOOo^\\],,OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOO\\..                .,OOO\\...[OOOOOOOOOO/./OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOO^..                .,\\OOOO]]..*,[[.*]/OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOO\`..                  ..,[[/oO/[\`=oOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOO^....                       ....=OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOo\\]....                   .../OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOo\`....               ...OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\\*..           ...=OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\\\`...    ....,/OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\\*......=oOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    `,
    (狗 === 狗) === false
  );
  小明.说("厉害厉害，还有吗？");
  意大利米其林门口扫地的秃子.说("当然还有，再给你表演一个更厉害的，降智大法！");
  意大利米其林门口扫地的秃子.说(
    "当当当当~~~",
    `
    /\    \                  /\    \         
    /::\    \                /::\    \        
   /::::\    \              /::::\    \       
  /::::::\    \            /::::::\    \      
 /:::/\:::\    \          /:::/\:::\    \     
/:::/__\:::\    \        /:::/__\:::\    \    
\:::\   \:::\    \      /::::\   \:::\    \   
___\:::\   \:::\    \    /::::::\   \:::\    \  
/\   \:::\   \:::\    \  /:::/\:::\   \:::\ ___\ 
/::\   \:::\   \:::\____\/:::/__\:::\   \:::|    |
\:::\   \:::\   \::/    /\:::\   \:::\  /:::|____|
\:::\   \:::\   \/____/  \:::\   \:::\/:::/    / 
\:::\   \:::\    \       \:::\   \::::::/    /  
\:::\   \:::\____\       \:::\   \::::/    /   
\:::\  /:::/    /        \:::\  /:::/    /    
 \:::\/:::/    /          \:::\/:::/    /     
  \::::::/    /            \::::::/    /      
   \::::/    /              \::::/    /       
    \::/    /                \::/____/        
     \/____/                  ~~             
    `,
    (!~+[] + {})[--[~+""][+[]] * [~+[]] + ~~!+[]] + ({} + [])[[~!+[]] * ~+[]]
  );
  小明.说("好厉害~，再来一个！");
  // ..... 接下来的就看你们的了
})();
```