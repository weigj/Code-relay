// 编程语言：java
// 规则： luck先说：幸运的是....
// !luck再说：不幸的是....；交替执行
// 故事主题：两个好朋友开启了一段对话......
public class Luck&!Luck { 
    Person luck = new Person();
    Person !luck = new Person();
    
    luck.say("幸运的是，今天周五了");
    !luck.say("不幸的是，产品提了一个新需求");

    luck.say("幸运的是，需求上写着下周实现");
    !luck.say("不幸的是，要求先上线测试版本")

    // ...
}