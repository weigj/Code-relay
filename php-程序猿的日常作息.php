<?php
// 编程语言：PHP
// 故事主题：程序猿的日常作息

class Person // 这个世界有人
{
    var $tired;
    var $thirsty;
    var $hungry;

    function Wake() { // 醒来
        $this->tired = 0;
        echo "早起精神充足<br>";
        $this->thirsty = 100;
        echo "好渴啊<br>";
        $this->hungry = 100;
        echo "有点饿<br>";
    }

  function Eat($food, $drink) { // 吃饭
    for ($i=0; $i < $this->hungry; $i++) {
        $this->hungry -= $food / 2;
        if ($this->hungry == 0) echo "吃饱了<br>";
    }

    for ($j=0; $j< $this->thirsty; $j++) {
        $this->thirsty -= $drink / 2;
        if ($this->thirsty == 0) echo "喝好了<br>";
    }
  }

  function Work($location, $job, $hour) { //工作
    for ($i=$hour; $i < 10; $i++) {
        $hour++;
        $this->tired++;
        echo "他在".$location."里".$job."中，已持续".$hour."小时<br>";
        if ($this->tired == 4) echo "午餐时间<br>";
        if ($this->tired == 7) echo "晚餐时间<br>";
        if ($this->tired == 10) echo "有点困了<br>";
    }
  }

  function Play($game, $hour) { //程序员怎么能不玩点游戏？
    echo "他开始玩${game}这个游戏了...<br>";
    echo "他已经玩了${hour}小时没有休息<br>";
  }
}

$programmer = new Person; // 这里有个程序员
$programmer->Wake(); // 早上了，起床啦
echo "来吃点早餐<br>";
$programmer->Eat(100,100); // 吃点早餐
echo "吃好早餐上班咯<br>";
$programmer->work("家","写代码",0); //上班ing
$programmer->Play('魔兽世界', 4); //累了，放松一下。
echo "睡觉吧，晚安<br>"; // 做完工作了，该睡觉了
?>
