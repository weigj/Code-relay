# 编程语言: Python
# 故事主题: 来到新的学校

mySelf = People()         # 我是一个人【滑稽】 
mySelf.name = '小刘'      # 我叫小刘
mySelf.age = 16          # 今年16岁
mySelf.gender = '男'     # 是一个男生

nbSchool = new heighShool()  # NB School 是一所高中
nbSchool.type = 'key school' # 这是一所重点中学！
nbSchool.classes = {
    '1班': {},
    '2班': {},
    '3班': {},
    '4班': {}
} # 这所学校一共有四个班级

nbSchool.classes['1班'].students = ['小张','小李','小王','小赵','小吴','小何']
nbSchool.classes['1班'].students.append(mySelf)  # 而我进入了1班
nbSchool.classes['1班'].teachers = ['李老师','王老师']

for student in nbSchool.classes['1班'].students:
        student.Introduce(self) # 每个同学都会介绍自己

for teacher in nbSchool.classes['1班'].teachers:
        teacher.Introduce(self) # 老师也挨个的进行了自我介绍

prettyGirl(nbSchool.classes['1班'].students,['小何'])    #小何是个很漂亮的女孩
richGirl(nbSchool.classes['1班'].students['小吴'])    #小吴是个富有的女孩子
studyhardGirl(nbSchool.classes['1班'].students['小赵'])  #小赵是个认真学习的女孩子

mySelf.height = 178     # 我身高一米七八
mySelf.weight = 126     # 体重126
mySelf.looks = '吴彦祖'  # 酷似吴彦祖

mySelf.deskmate = random.choose(nbSchool.classes['1班'].students)  #要给安排同桌啦

curriculum = {      # 这是我们班周一到周五的课表
    "周一": ['语文','英语','数学','体育','班会','物理'],
    "周二": ['化学','语文','生物','英语','英语','数学'],
    "周三": ['数学','历史','地理','物理','编程','自习'],
    "周四": ['英语','体育','化学','物理','语文','语文'],
    "周五": ['数学','语文','英语','选修','选修','放学']
}

for date in ['周一','周二','周三','周四','周五']:  # 一周有五天要上课 (可以在里面加更多小插曲)
    for nclass in date:
        attendClass(nclass,nbSchool.classes['1班'].students)
        time.sleep(10 * 60) # 每节课后，有十分钟的休息时间啦
        backToDormitory(nbSchool.classes['1班'].students) # 一天的课程结束后，我们会回到寝室准备睡觉
        goToSleep(nbSchool.classes['1班'].students) # 进入梦乡，等待第二天的到来
        time.sleep(10 * 60 * 60) # 此时我们可以休息 10个小时呢

goHome(nbSchool.classes['1班'].students) # 一周的课程结束了