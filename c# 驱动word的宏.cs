            WinWord.Application oApp = new WinWord.Application();
            oApp.Visible = true;
            if (System.IO.File.Exists(@"D:\Demo\Test_Demo.pdf")) System.IO.File.Delete(@"D:\Demo\Test_Demo.pdf");
            if (System.IO.File.Exists(@"D:\Demo\Test_Demo.docx")) System.IO.File.Delete(@"D:\Demo\Test_Demo.docx");
            System.IO.File.Copy(@"D:\Demo\Test.docx", @"D:\Demo\Test_Demo.docx");
            WinWord.Document oDoc = oApp.Documents.Open(@"D:\Demo\Test_Demo.docx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            
            VB.VBComponent oModule = oDoc.VBProject.VBComponents.Add(VB.vbext_ComponentType.vbext_ct_StdModule);
            string sCode = System.IO.File.ReadAllText("Script.vba");//读取脚本
            Console.Write(sCode);
            oModule.CodeModule.AddFromString(sCode);
            oApp.Run("MK");
            //oDoc.SaveAs2(@"D:\Demo\Test_Demo.pdf", WinWord.WdExportFormat.wdExportFormatPDF);
            oDoc.ExportAsFixedFormat(@"D:\Demo\Test_Demo.pdf", WinWord.WdExportFormat.wdExportFormatPDF, true, WinWord.WdExportOptimizeFor.wdExportOptimizeForPrint, WinWord.WdExportRange.wdExportAllDocument, 0, 0, WinWord.WdExportItem.wdExportDocumentContent, true, true, WinWord.WdExportCreateBookmarks.wdExportCreateWordBookmarks);
            //ObjExcelWorkBook.ExportAsFixedFormat(
            //        Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF,
            //        Environment.CurrentDirectory + @"\temp\" + 编辑页面.ToString() + 文件类型,
            //        Microsoft.Office.Interop.Excel.XlFixedFormatQuality.xlQualityStandard, Type.Missing, false, Type.Missing, Type.Missing, false, Type.Missing);
            //oApp.GetType().InvokeMember("Run", System.Reflection.BindingFlags.Default | System.Reflection.BindingFlags.InvokeMethod, null, oApp, new object[] { "M1" });//运行已有脚本
            oDoc.Close(Type.Missing,Type.Missing,Type.Missing);
            oApp.Quit();
            GC.Collect();//CleanMemery