![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/235754_10b4f357_1899542.png "图片.png")

### 活动介绍
第 0  届 代码接力编故事挑战赛开始啦！  
       
让我们点燃程序员之魂，打开脑洞，一人一段接力用代码写故事玩玩吧！  
  
活动时间：2020年3月23日 - 2020年3月30日    

现在已经有很多有意思的故事产出，为了更方便大家接力，我们将按类型整理在这里，快来接力吧！
|  编程语言   | 故事主题  | 
| --- | --- | 
|  c#  |   [TimeTraveller（时间旅行者）](https://gitee.com/Selected-Activities/Code-relay/blob/master/c%23-TimeTraveller.md)  |     |
|  c#   |  [我要当老板](https://gitee.com/Selected-Activities/Code-relay/blob/master/c%23%E6%88%91%E8%A6%81%E5%BD%93%E8%80%81%E6%9D%BF)   |     
| c#    | [驱动word的宏](https://gitee.com/Selected-Activities/Code-relay/blob/master/c%23%20%E9%A9%B1%E5%8A%A8word%E7%9A%84%E5%AE%8F.cs)    | 
| c#    | [BabySayHelloWorld](https://gitee.com/Selected-Activities/Code-relay/blob/master/C%23-BabySayHelloWorld.md) | 
| c#    | [bug开发人员](https://gitee.com/Selected-Activities/Code-relay/blob/master/C%23-bug%E5%BC%80%E5%8F%91%E4%BA%BA%E5%91%98)  | 
|  c++    |  [两个和尚](https://gitee.com/Selected-Activities/Code-relay/blob/master/c++-%E4%B8%A4%E4%B8%AA%E5%92%8C%E5%B0%9A.cpp)   |     
|   c++  |  [偷偷把公司的AI拿了出来](https://gitee.com/Selected-Activities/Code-relay/blob/master/C++%E5%81%B7%E5%81%B7%E6%8A%8A%E5%85%AC%E5%8F%B8%E7%9A%84AI%E6%8B%BF%E4%BA%86%E5%87%BA%E6%9D%A5)   |     
|   c++  |  [玲旦愿](https://gitee.com/Selected-Activities/Code-relay/blob/master/c++-%E7%8E%B2%E6%97%A6%E6%84%BF.cpp)  |     
|  C   |   [AbadStory](https://gitee.com/Selected-Activities/Code-relay/blob/master/c_AbadStory.c)   | 
|  java |  [AStrangeMan](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-AStrangeMan.md)  |
|  java |  [i`m in computer](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%20i%60m%20in%20computer) |
|  java |  [砍柴的王小二](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-FirewoodChopperWangXiaoEr.md)  |
|  java |  [Flatterer(舔狗)](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-Flatterer(%E8%88%94%E7%8B%97).java)  |
|  java |  [臭阿姨大战LBW](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-LBWNB.md)  |
|  java |   [喜羊羊与灰太狼](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-PleasantGoatAndBigBigWolf.md) |
|  java | [从前有座山……](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-StoryMountainTest)   |
|  java |  [gettingBald(秃)](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-gettingBald(%E7%A7%83).md)  |
|  java |  [这天，命运的齿轮开始转动了...](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-nina)  |
|  java |  [阿里巴巴的续集……](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-street.md)  |
|  java |  [当你借用你朋友的电脑提交代码时...](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-submityourcode)  |
|  java | [两个和尚](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E4%B8%A4%E4%B8%AA%E5%92%8C%E5%B0%9A.md)   |
|  java |  [关公战秦琼](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E5%85%B3%E5%85%AC%E6%88%98%E7%A7%A6%E7%90%BC.md)  |
|  java |  [码神传奇之从出生开始就会写代码](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E7%A0%81%E7%A5%9E%E4%BC%A0%E5%A5%87%E4%B9%8B%E4%BB%8E%E5%87%BA%E7%94%9F%E5%BC%80%E5%A7%8B%E5%B0%B1%E4%BC%9A%E5%86%99%E4%BB%A3%E7%A0%81.md)  |
|  java |  [钢铁直男](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E9%92%A2%E9%93%81%E7%9B%B4%E7%94%B7.md)  |
|  java | [我的真人创业故事](https://gitee.com/Selected-Activities/Code-relay/blob/master/java_MyRealStory.java)   |
|java|[grandMother](https://gitee.com/Selected-Activities/Code-relay/blob/master/Java-grandMother.java)|
|java|[Gullum's Ring（grandMother 续写）](https://gitee.com/Selected-Activities/Code-relay/blob/0d122d0917349de1837363f25755defefce6d671/Java-Gullum's%20Ring.java)|
|java|[helloWorld你们懂的](https://gitee.com/Selected-Activities/Code-relay/blob/master/helloWorld%E4%BD%A0%E4%BB%AC%E6%87%82%E7%9A%84.java)|
|java| [Trump变形记](https://gitee.com/Selected-Activities/Code-relay/blob/master/Java-ReverseTrump)|
|java| [啊，不要](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E5%95%8A%EF%BC%8C%E4%B8%8D%E8%A6%81.md) |
|java| [川普熔断](https://gitee.com/Selected-Activities/Code-relay/blob/master/java_%E5%B7%9D%E6%99%AE%E7%86%94%E6%96%AD.java) |
|java| [杀手小黑的佛系生活](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E6%9D%80%E6%89%8B%E5%B0%8F%E9%BB%91%E7%9A%84%E4%BD%9B%E7%B3%BB%E7%94%9F%E6%B4%BB) | 
|java| [红色波浪线](https://gitee.com/Selected-Activities/Code-relay/blob/master/Java%20-%20%E7%BA%A2%20%E8%89%B2%20%E6%B3%A2%20%E6%B5%AA%20%E7%BA%BF) |   
|java | [幸与不幸（两个好朋友开启了一段对话...）](https://gitee.com/Selected-Activities/Code-relay/blob/master/java_%E5%B9%B8%E4%B8%8E%E4%B8%8D%E5%B9%B8.java) |
|java | [舔狗日记](https://gitee.com/Selected-Activities/Code-relay/blob/master/%E8%88%94%E7%8B%97%E6%97%A5%E8%AE%B0.java) |
|java |  [第一代bug的诞生](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E7%AC%AC%E4%B8%80%E4%BB%A3bug%E7%9A%84%E8%AF%9E%E7%94%9F.md)|
|java | [java之华山论剑](https://gitee.com/Selected-Activities/Code-relay/blob/master/java%E4%B9%8B%E5%8D%8E%E5%B1%B1%E8%AE%BA%E5%89%91.java) |
|java | [凭什么用Java的人最多](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E5%87%AD%E4%BB%80%E4%B9%88%E7%94%A8Java%E7%9A%84%E4%BA%BA%E6%9C%80%E5%A4%9A) |
|  js | [娱乐幽默（布由的校园故事）](https://gitee.com/Selected-Activities/Code-relay/blob/master/JS-DailyStory.js)  |   
|  js | [程序人生](https://gitee.com/Selected-Activities/Code-relay/blob/master/coding_lief(%E7%A8%8B%E5%BA%8F%E4%BA%BA%E7%94%9F).js)  |   
| js  | [奇妙遨游](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-MarvelousTour)  |   
| js  |  [一个悲伤的故事](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-a-sad-story) |
|  js | [ghost_story(正儿八经的鬼故事)](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-ghost_story(%E6%AD%A3%E5%84%BF%E5%85%AB%E7%BB%8F%E7%9A%84%E9%AC%BC%E6%95%85%E4%BA%8B))  |  
|  js | [缘之空-After](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-yosuganosoraAfter)  | 
|  js |  [小和尚和老师傅](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-%E5%B0%8F%E5%92%8C%E5%B0%9A%E5%92%8C%E8%80%81%E5%B8%88%E5%82%85) | 
|  js | [灵猴降世](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-%E7%81%B5%E7%8C%B4%E9%99%8D%E4%B8%96.md) | 
|  js | [猴子捞月](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-%E7%8C%B4%E5%AD%90%E6%8D%9E%E6%9C%88.md) |
|  js | [鼠年春节，我的旅程因新冠疫情变得离奇](https://gitee.com/Selected-Activities/Code-relay/blob/master/index.js) |
|  js | [河马船长奇遇记](https://gitee.com/Selected-Activities/Code-relay/blob/master/%E6%B2%B3%E9%A9%AC%E8%88%B9%E9%95%BF%E5%A5%87%E9%81%87%E8%AE%B0)  |
|  js | [少年我看你骨骼精奇，进来看看吧](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-%E5%B0%91%E5%B9%B4%E6%88%91%E7%9C%8B%E4%BD%A0%E9%AA%A8%E9%AA%BC%E7%B2%BE%E5%A5%87%EF%BC%8C%E8%BF%9B%E6%9D%A5%E7%9C%8B%E7%9C%8B%E5%90%A7~~.md) | 
|  js | [两头牛](https://gitee.com/Selected-Activities/Code-relay/blob/master/two%20cows.js) |
|  js | [生物人工智能，有人进微信群以后](https://gitee.com/Selected-Activities/Code-relay/blob/master/js%E7%94%9F%E7%89%A9%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%EF%BC%8C%E6%9C%89%E4%BA%BA%E8%BF%9B%E5%BE%AE%E4%BF%A1%E7%BE%A4%E4%BB%A5%E5%90%8E) |   
|  js | [他（官方说明：内容可能引起不适，谨慎点入）](https://gitee.com/Selected-Activities/Code-relay/blob/master/js-%E4%BB%96)|
 |  Python |[遇见小狗](https://gitee.com/Selected-Activities/Code-relay/blob/master/python-a_day_of_little_dog) |
 |  Python | [小董和小王的爱情故事](https://gitee.com/Selected-Activities/Code-relay/blob/master/python-love-story.py)|
 |  Python | [来到新的学校](https://gitee.com/Selected-Activities/Code-relay/blob/master/python-my_high_school.py)|
 |  Python | [小王和网课的故事](https://gitee.com/Selected-Activities/Code-relay/blob/master/python-onlinestudy.py)|
 |  Python |[我们的程序员生活](https://gitee.com/Selected-Activities/Code-relay/blob/master/python3-Our_Programmer_Life.py) |
|  Python | [时间有声音吗？也许他有...](https://gitee.com/Selected-Activities/Code-relay/blob/master/listen_time.py)|
|  Python |  [编程交流群里面突然来了一个用户名叫“Creeper”的网友，于是......](https://gitee.com/Selected-Activities/Code-relay/blob/master/python-creeper.py)|
|  Python | [算命](https://gitee.com/Selected-Activities/Code-relay/blob/master/python-%E7%AE%97%E5%91%BD) |
|  Python | [霍格沃茨入学仪式极简版](https://gitee.com/Selected-Activities/Code-relay/blob/master/Python-%E9%9C%8D%E6%A0%BC%E6%B2%83%E8%8C%A8%E5%85%A5%E5%AD%A6%E4%BB%AA%E5%BC%8F%E6%9E%81%E7%AE%80%E7%89%88.py)|
|  PHP | [金庸小说考题试卷](https://gitee.com/Selected-Activities/Code-relay/blob/master/PHP-examination)|
 |  PHP | [生存或是死亡(欢迎其它语言接力)](https://gitee.com/Selected-Activities/Code-relay/blob/master/%E7%94%9F%E5%AD%98%E6%88%96%E6%98%AF%E6%AD%BB%E4%BA%A1(%E6%AC%A2%E8%BF%8E%E5%85%B6%E5%AE%83%E8%AF%AD%E8%A8%80%E6%8E%A5%E5%8A%9B).php)|
|  PHP | [AI](https://gitee.com/Selected-Activities/Code-relay/blob/master/php%20-%20AI)|
 |  PHP | [php7.4之脱单之路](https://gitee.com/Selected-Activities/Code-relay/blob/master/php7.4%E4%B9%8B%E8%84%B1%E5%8D%95%E4%B9%8B%E8%B7%AF)|
|  PHP |  [小狗变成小猫](https://gitee.com/Selected-Activities/Code-relay/blob/master/php-%E5%B0%8F%E7%8B%97%E5%8F%98%E6%88%90%E5%B0%8F%E7%8C%AB.php)|
|  PHP | [程序猿的日常作息](https://gitee.com/Selected-Activities/Code-relay/blob/master/php-%E7%A8%8B%E5%BA%8F%E7%8C%BF%E7%9A%84%E6%97%A5%E5%B8%B8%E4%BD%9C%E6%81%AF.php) |
|  PHP |[PHP说科学家的故事](https://gitee.com/Selected-Activities/Code-relay/blob/master/PHP%E8%AF%B4%E7%A7%91%E5%AD%A6%E5%AE%B6%E7%9A%84%E6%95%85%E4%BA%8B.php)|
|  go | [考试的故事](https://gitee.com/Selected-Activities/Code-relay/blob/master/%E8%80%83%E8%AF%95%E7%9A%84%E6%95%85%E4%BA%8B-go)| 
|  go | [下一场二进制雨](https://gitee.com/Selected-Activities/Code-relay/blob/master/golang-bit_rain.go)| 
|  XAE（一种插件语言） | [那天，我看到了世界的尽头...](https://gitee.com/Selected-Activities/Code-relay/blob/master/XAE-Worldside)| 
| dart| [这个人进行了自我介绍](https://gitee.com/Selected-Activities/Code-relay/blob/master/dart-selfIntroduction.dart)|
| kotlin | [肉摊的故事](https://gitee.com/Selected-Activities/Code-relay/blob/master/demo.kt)  |
| kotlin | [一个程序员大男孩与一位清秀女孩邂逅，然后他们交往了......](https://gitee.com/Selected-Activities/Code-relay/blob/master/StoryForCoder) |
| wenyan-lang|[邻家姑娘叫小花](https://gitee.com/Selected-Activities/Code-relay/blob/master/wy-%E9%82%BB%E5%AE%B6%E5%A7%91%E5%A8%98%E5%8F%AB%E5%B0%8F%E8%8A%B1.wy) |
| 易语言| [上网惊魂](https://gitee.com/Selected-Activities/Code-relay/blob/master/%E6%98%93%E8%AF%AD%E8%A8%80-%E4%B8%8A%E7%BD%91%E6%83%8A%E9%AD%82)|
       
     
### 活动规则
      
你可以自行发起一个故事，也可参与他人发起的故事，进行代码编故事接力     
    
编程语言：故事可以用任何编程语言描述，包括伪代码

增加一条规则：大家请文明用语哈~不然不会通过噢        
  
**发起故事**      
  
- 发起一段故事时，请取一个合适的故事名，文件名长度不得超过 20 个字符（不含扩展名），不能和他人的文件重名（文件命名格式：编程语言-故事名.xxx）    
    
- 发起一段故事时，需通过注释规定故事编程语言、交代主题，让接力者能更明白你想要表达的意思           
      
 **接力故事**        
  
- 参与者在参与接力时，不允许修改、删除代码，只允许以代码+注释的方式，在已有代码的基础上增加一段代码（在一个故事中，参与者添加的代码不能超过 5 行，添加的注释尽量写在一行内，不要超过3行，代码可以添加在已有代码的任意位置）             
         
- 参与接力时，请务必为你提交的代码的给出详细的注释、说明       
   
- 提交的代码尽量不要有bug，非必须符合代码规范，但必须符合逻辑，最好可正常运行                        
  
----------------------------------

故事发起格式参考：
```
 //编程语言：java
 //故事主题：这天，一个奇怪的人出现了...

import java.util.ArrayList;

public class StoryTest {
    public static void main(String[] args) {
        Story story = new Story();
        List<Person> personList = new ArrayList();
        story.setTime("night");      //一个月黑风高的晚上
        story.setAddress("lawn");    //我在草坪上散步

        Person person = new Person();
        personList.add(person);      //突然有个人向我走来

        Person personMan = new Person();
        personMan.setBody("strong");
        personMan.setHigh("1.8m");
        personList.add(personMan);    //那个人身材魁梧，一米八几的样子

        story.setPersonList(personList);

        person.setStatus("scared");  
        person.say("你！你是……");     //我被吓坏了，惊道："你！你是……"
    }
}
..... // 接下来的就看你们的了
 } 
``` 




### 参与方式
由于 Gitee 轻量级 PR 功能的上线，参与本活动无须再 Fork 仓库，大家可以直接在本仓库完成具体的操作，具体方式请看下方：  

#### 发起故事
在本仓库内选择【新建文件】发起一个故事，文件命名格式：编程语言-故事名.md  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0320/172836_ab68f94f_1899542.png "1.png")        
      
写好后，点击“ 提交审核 ”，提交 PR  进入审核        
![输入图片说明](https://images.gitee.com/uploads/images/2020/0320/172923_c1f88210_1899542.png "2.png")      
   

#### 故事接力   
点击一个故事参与接力      
![输入图片说明](https://images.gitee.com/uploads/images/2020/0320/173146_1920eb03_1899542.png "3.png")      
    
点击【编辑】，添加代码及注释        
![输入图片说明](https://images.gitee.com/uploads/images/2020/0320/173251_d3674d36_1899542.png "4.png")  
      
点击“提交审核”，提交 PR 进入审核           
![输入图片说明](https://images.gitee.com/uploads/images/2020/0320/173320_7953b810_1899542.png "5.png")    

 
#### 活动主办方
![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/113334_c210312f_1899542.png "172-60.png")    
  
#### 支持社区    
![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/113728_4873e69a_1899542.png "开源中国.png")          
![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/114154_d027035a_1899542.png "segmentfault.png")   
![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/114113_31ef43ec_1899542.png "181839_8huZ_2686220.png")    

#### 活动奖品（有奖活动已结束，名单请见下方）
为了鼓励大家踊跃参与，我们也准备了一些小礼物送给大家  
  
**对故事发起者的奖励：**   
前 5 位发起故事并通过审核的小伙伴将直接获得 Gitee 2020 新版官方T恤 1 件           
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/160747_e22cb036_1899542.png "GiteeT恤.png")                      
    
故事被接力次数最多前 3 名，将获得 蒲公英VPN.智能旁路盒子 + Gitee 2020 新版官方 T 恤 1 份 （PS：欢迎邀请小伙伴为你的故事接力喔）          
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/160004_d037b570_1899542.png "T恤+智能盒子.png") 

**对故事接力者的奖励：**

活动结束后，我们将抽取 15 位参与接力的小伙伴送出 Gitee 2020 新版官方T恤     
  
获奖公布时间：2020年4月1日 （公布名单后，Gitee酱将私信联系你，注意留意哟~） 

---------------------------------------------------------------------

 **获奖名单公布**   
恭喜故事被接力次数最多前 3 名：  
 [ljzone](https://gitee.com/ljzone)，[Jixq](https://gitee.com/jixq)，[intxiaoquan](https://gitee.com/intxiaoquan)，获得蒲公英VPN.智能旁路盒子 + Gitee 2020 新版官方 T 恤 1 份   

编程语言：C++
故事名：[玲旦愿  （枯燥老总爱上扫地玲 ...）](https://gitee.com/Selected-Activities/Code-relay/blob/master/c++-%E7%8E%B2%E6%97%A6%E6%84%BF.cpp)
故事发起者：ljzone 

编程语言：java
故事名：[砍柴的王小二](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-FirewoodChopperWangXiaoEr.md) 
故事发起者：Jixq

编程语言：java
故事名：[杀手小黑的佛系生活](https://gitee.com/Selected-Activities/Code-relay/blob/master/java-%E6%9D%80%E6%89%8B%E5%B0%8F%E9%BB%91%E7%9A%84%E4%BD%9B%E7%B3%BB%E7%94%9F%E6%B4%BB)  故事发起者： intxiaoquan
   
      
恭喜前 5 位发起故事并通过审核的小伙伴：  
[郑壮](https://gitee.com/zhengzhuang)，[TheInterestingSoul](https://gitee.com/theinterestingsoul) ，[meetqy](https://gitee.com/meetqy) ，[bao101](https://gitee.com/bao101)，[Gaoy9303](https://gitee.com/Gaoy9303) ，喜提 Gitee 2020 新版官方 T 恤 1 件    
      
另外，也恭喜以下 15位参与接力的小伙伴，喜提 Gitee 2020 新版官方 T 恤 1 件     
[吉吉](https://gitee.com/mmdjiji)， [jing](https://gitee.com/J_Projects)，[睿若](https://gitee.com/xiaoruiruo)，[xhemj](https://gitee.com/xhemjHeYanju) ， [赵芝士](https://gitee.com/defangit_zhaozhishi) ，[小鱼儿](https://gitee.com/xiaour) ，[loveBug](https://gitee.com/hmmabc) ，[殷晓苗](https://gitee.com/yin_xiao_miao) ， [Jason23347](https://gitee.com/jason23347)，[豆豆雨](https://gitee.com/shyr) ，[赫尔heer](https://gitee.com/heerkaisair) ，[EProKing](https://gitee.com/eproking)，[程序小黑](https://gitee.com/wangdudyb)，[阿前666](https://gitee.com/aqian666)，[mr小卓X](https://gitee.com/mrxzx)
     
  
我们将在2个工作日内私信联系你们，注意留意私信哟~ 恭喜中奖！（若在5月1日前未收到回复则默认为放弃领取奖品）       
为了避免有遗漏的情况，大家也可以直接将邮寄信息私信给 [@小鱼丁](https://gitee.com/xiaoyuding)

      

    
   
 