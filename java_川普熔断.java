// 编程语言：java
// 故事主题：看到川普，想起了高兴地事.....
public class TrumpCircuitBreaker {

  public static void main(String[] args) {

    President trump = new President("金色刘海");
    WorldDate date = new WorldDate();
    int circuitBreakCount = 0;

    // 川普熔断第一次
    date.today("2020-03-08");
    trump.twitter("美国确诊这么少归功于我，彭斯一天工作20小时(https://b23.tv/BV1hE411p72M)");
    this.circuitBreakCount = 1;

    // 川普熔断第二次
    date.today("2020-03-12");
    trump.twitter("没理由做检测，我身体棒极了(https://b23.tv/BV1CE411K7zz)");
    this.circuitBreakCount = 2;

    // 川普熔断第三次
    date.today("2020-03-16");
    trump.twitter("美国检测能力滞后我不会为此负责(https://b23.tv/BV1sE411V7Dh)");
    this.circuitBreakCount = 3;

    // 川普熔断第四次
    date.today("2020-03-18");
    trump.twitter("我早知道这是全球大流行病(https://b23.tv/BV1t7411o7sb)");
    this.circuitBreakCount = 4;

    // 川普熔断第五次
    date.today("2020-03-27");
    trump.twitter("美国可能面临历史上最大的灾难(https://b23.tv/BV1T7411N75L)");
    this.circuitBreakCount = 5;

    //作者请勿见怪
    /**
    date.today("2021-03-27");
    trump.twitter("美联储倒闭("https://b23.tv/404")");
    this.circuitBreakCount = Math.pow(this.circuitBreakCount, this.circuitBreakCount);
    **/

    // 未完待续....
    
    // 遥遥领先的语言
    date.today("2020-04-01");
    trump.twitter("复活节大家一起聚餐哦");
    this.circuitBreakCount = 6;

  }

}