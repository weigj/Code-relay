

``` java 
     /**
     * 编程语言：java
     * 故事主题：两个和尚
     */
    public static void main(String[] args) throws InterruptedException {
        
        
        // 从前有个山，山里有个庙
        //庙里有个老和尚
        //老和尚有一个理想
        String dream = "无论是炎夏或寒冬，我都很向往山门外的天空，还在北方等我下山的我的人叫小...洛";
        Person lhs = new Person();
        lhs.setAge(78);
        lhs.setDream(dream);
        //和小和尚
        Person xhs = new Person();
        xhs.setAge(16);

        //有一天，小和尚说
        xhs.say("师傅师傅，给我讲个故事吧");
        //老和尚回答
        lhs.say("好的徒儿！故事要为师看到的一条动弹说起，有人说要用代码写故事，写的是什么故事呢？");

        while (80 >= lhs.getAge) {
            sayStory();
            TimeUnit.HOURS.sleep(24);

        }
        //老和尚不知不觉中BB了两年，在80岁的这一天去世了
        //小和尚悲痛欲绝，然后。。。。
        
        //小和尚隐约想起了师傅从前的梦想
        lhs.getDream;
        //小和尚恍然大悟，对着死去的师傅说
        xhs.say("师傅您给我讲这个故事就是为了让我练就绝世武功，要忍受常人难忍受的痛，然后实现您的理想，徒儿不会辜负您的期望，帮您实现未        
        完成的愿望");
        //小和尚大喊一声
        xhs.say("老和尚再见");
        //此时小和尚也有了自己的理想
        xhs.setDream(dream);
        //小和尚怀揣着梦想下了山，边走边唱
        while (20 >= xhs.getAge) {
            xhs.say(dream);
            TimeUnit.HOURS.sleep(24);
        }
        //不知不觉，小和尚走了两年，唱了两年...
        //在这天，他终于...
        
        thunder(); //突然一声巨雷，将小和尚拉回现实
        xhs.say("早知道是这样，像梦一场，我才不会把梦放在同一个地方");
        //说完小和尚背上行囊
        xhs.goDown(); //下山远去...
    }

    private static void sayStory() {
        System.out.println("从前有个山，山里有个庙," +
                "庙里有个老和尚和小和尚," +
                "有一天，老和尚给小和尚讲故事,嘿，" +
                "有人要用代码写故事，写的是什么故事呢？" +
                "从前有个山，山里有个庙.....");
    }
```
