 //编程语言：java
 //故事主题：父母外出工作，只留下两个小女儿在家，当夜幕降临...
import java.text.SimpleDateFormat;

public class OldStory{
    public static void main(String[] args) {
        // 一家人有爸爸妈妈，两个女儿，大的14岁，小的7岁，还有70岁的外婆
        Family family = new Family();
        family.addFather(new Father());
        family.addMother(new Mother());
        Sister bigSister = new BigSister();
        bigSister.setAge(14);
        family.addSister(bigSister);
        Sister smallSister = new SmallSister();
        smallSister.setAge(7);
        family.addSister(smallSister);
        GrandParent gp = new GrandMother();
        gp.setAge(70);
        family.setGrandMother(gp);

        // 熊嘎婆（噶婆=外婆）
        UnknownGost gost = new UnknownGost();
        gost.setName("熊嘎婆");
        Listener listener = new AroundListener();
        listener.setWidth(1 * 1000);
        gost.setListener(listener);
        gost.listen();

        // 爸妈出远门
        WorkTask task = new WorkTask();
        task.addPerson(family.getFather());
        task.addPerson(family.getMother());
        task.setAddress("Far Away From Home");
        task.setWorkTime("After sunset");

        // 当夜幕降临。。。
        Word.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-03-23 19:00:00"));
        // 温度也降的很低
        Word.setTemperature(0);

        // 父母叮嘱女儿们要关好门窗，并让外婆过来照顾两个小女儿
        family.getMother().addNotice("爸爸妈妈要工作去了，晚上让外婆过来照顾你们，要关好门窗，不让陌生人进来！");
        task.run();

        // 熊嘎婆听到这件事
        gost.stopListenAndGetResult();
        // 熊嘎婆等待外婆的到来
        gost.waitting();

        try{
            // 外婆去照顾两个孙女
            gp.goTo(family.getLocation());
        }catch(BeFound e){
            // 熊嘎婆发现了外婆，并吃掉了她，然后披着她的皮去找两个小女儿了
            gost.eat(gp);
            gost.setSkin(gp.getSkin());
            gost.goTo(gp.getTargetPlace());
        }

        // 女儿们把门窗都关好
        bigSister.lockWindows();
        smallSister.lockDoors();

        // 烤火等外婆过来
        Action action = new Action();
        action.addEvent("在灶门前烤火");
        action.addEvent("关灯");
        action.addPerson(bigSister);
        action.addPerson(smallSister);
        action.submit();

        // 熊嘎婆到了，敲门并说道：
        gost.bit(family.getFrontDoor());
        gost.say("妹儿，嘎婆来照顾你们了，快开开门");

        // 大女儿透过门缝看到是外婆，就开了门
        bigSister.see(family.getFrontDoor().getDoorSeam());
        bigSister.check(gost);
        bigSister.open(family.getFrontDoor());
        
        // 熊嘎婆怕被发现了，就说：
        gost.say("别开灯，爸爸妈妈上班不容易，要节约一些，没什么事就早点睡觉吧");

        // 小女儿很开心，但大女儿却有点怀疑了
        smallSister.setHappy(true);
        bigSister.setDoubt(true);

        // 小女儿和熊嘎婆睡一起
        Bed bed1 = family.getOneBed();
        bed1.addPerson(gost);
        bed1.addPerson(smallSister);

        family.getOneBed().addPerson(bigSister);

        // 深夜，大女儿被一阵阵噪声吵醒
        bigSister.wakeUp(new Noise().setVoice("噶，噶，噶~"));

        // 于是大女儿去找外婆，大发现她在吃着什么东西，就问道：
        bigSister.goTo(bed1.getLocation());
        bigSister.say("外婆，你在吃什么?");

        gost.say("我在吃家里带过来的干胡豆");
        bigSister.say("我也要吃");

        // 熊嘎婆顺手递过去了“干胡豆”
        gost.give(bigSister, smallSister.getFingers().getForefinger());

        // 大女儿定睛一看，没想到这居然是妹妹的手指！！！ @A@
        bigSister.setPanic(true);
        
        // 年迈慈祥的外婆竟是熊嘎婆，孤单的大女儿将何去何从？
        // 欢迎大家继续补充 

        //一枚戒指从妹妹的手指脱落...
        Ring ring = new Ring(smallSister,smallSister.getBoold(),"diamond");
        //屋内的温度似乎上升些许...
        Word.setTemperature(Math.random());
        //戒指掉在地上发出一点光芒
        Floor floor = ROOM.floor;
        floor.crushedBy(ring,"ding~ding~ding~");
        ring.beginBlink();
        //地板隐藏的秘密....
        floor.sign("Ring'Method: SetOwner(Person pserson),setLook(String look),beginBlink(),resurgenceOwner(Boold killer,....");
        bigSister.getRecollect(new Book("How to Kill others"));
        
        //大女儿打开刚捡到的书，富有兴致地读了起来
        bigSister.open(books.pop());
        Poison poison = new Poison();
        poison.add("KCN");
        bigSister.putPoison(smallSister.getFingers().getForefinger(),poison);//“干胡豆”
        
        //第二天早上，大女儿把“干胡豆”递给熊嘎婆，示意她吃
        //究竟后事如何，请听下回分解

        //...
    }
}