import java.util.ArrayList;
import java.util.List;

/**
* 这是一个属于我的真人故事
* 以下这段代码可以直接运行，可以运行一下，看看我的故事
* 说好的合伙创业，却成了一个人的企业
* 朋友们，有创业想法的一定要自己干，合伙创业就是个坑
*/
public class MyRealStory {
    public static void main(String[] args) {
                                                                 //曾经有4个人,分别是
        Person a = new Person("A", "结构");                      //A是个机械结构工程师
        Person b = new Person("B", "销售");                      //B是个销售
        Person c = new Person("C", "单片机");                    //C是个电气工程师（单片机/下位机）
        Person d = new Person("D", "上位机");                    //D是上位机软件工程师（应用层软件开发）
        a.teamUp(b, c, d);                                      //A将这些人组织了起来
        a.say("Let's start a business together");               //我们合伙创业吧
        a.getAllPeople("OK");                                   //所有人都同意了
        Company company = new Company(a, b, c, d);
        company.invest(a, 150);                                 //后来，A投资了150W,成了大股东
        company.invest(b, 30);                                  //B投了30W  成了第二大股东
        company.invest(c, 0);                                   //C没有投钱
        company.invest(d, 5);                                   //D投了5W
        a.promise(b, "500万销售额", 0.02);                       //A承诺B，只要一年销售额达到500万，就给他2%的技术股
        a.promise(c, "1项新技术的研发", 0.02);                    //A承诺C,只要完成一项新技术的研发，就给他2%的技术股
        a.promise(d, "8篇软著文档", 0.02);                       //A承诺D,只要完成8篇软著，给他2%技术股
        a.getAllPeople("OK");                                   //所有人答应了
        a.getAllPeople("加油");                                 //他们纷纷全职出来了，为了梦想，为了希望，他们满怀信心经营这家新公司
        c.development();                                        //C开始搞研发
        d.developmentWith(c);                                   //D在C的基础上研发上位机软件
        Customer customer = new Customer("XX");
        b.getOrder(customer);                                   //B接到客户的项目
        a.design();                                             //由于是非标项目，A负责设计外形
        b.construction(c);                                      //B C 去现场施工
        System.out.println("But");                              //但是
        customer.say("ugly and Unpractical");                   //客户嫌弃设计的难看，以及不实用，设计时都不考虑现场情况
        System.out.println("Final");                            //最终
        customer.refund();                                      //客户选择退款
        a.ability(0.2);                                         //A设计能力有问题+管理能力有问题
        company.project(false);                                 //导致很多项目失败

        for (int i = 0; i < 18; i++) {                          //资金日渐消耗
            company.setMoney(company.getMoney() - 10);
        }
        System.out.println(company.getMoney() < 10 ? "缺钱" : "不缺"); //资金也快烧光了
        Person e = new Person("E", "投资人");
        a.addInvest(e);                                        //A去拉投资，引进新的资本
        d.setShare(d.getShare() - 0.01);                       //可是却把D的股份降了 当时的承诺呢，一份法律文件说改就改
        d.quarrel(a);
        d.quit();                                              //D跟他吵了一架，走了（辞职了）
        a.call(d, "  back");                                   //但由于招不到人，在退股，退投资的前提下，把D找了回来
        e.say("MOVE NEW PLACE");                               //在引入新资本后，E成为了大股东，在他的管理下，搬了新的办公地
        c.quit("Family");                                      //C由于家庭的无奈，无法跟着公司一起搬地址,就这样走了
        d.hateAndQuit(a);                                      //但由于AD之间的矛盾，积累太深，最终无奈，D还是离开了(中间的过程省略)
        company.getTeam("disperse");                           //曾经的团队，也因此分散

        System.out.println("I'm the D ");                      //没错，我就是那个D

        d.say(a, "加油");                                      //我现在只能对A说：加油
    }


    static class Company {
        private int money;
        private List<Person> team;

        public Company(Person a, Person b, Person c, Person d) {
            money = 0;
            System.out.println(a.getName() + "加入了公司");
            System.out.println(b.getName() + "加入了公司");
            System.out.println(c.getName() + "加入了公司");
            System.out.println(d.getName() + "加入了公司");
            team = new ArrayList<Person>();
            team.add(a);
            team.add(b);
            team.add(c);
            team.add(d);

        }

        public void invest(Person person, int money) {
            this.money += money;
            System.out.println(person.getName() + "投了" + money + "万元");
        }

        public void project(boolean b) {
            System.out.println(b ? "项目成功" : "失败");
        }

        public int getMoney() {
            return money;
        }

        public void setMoney(int i) {
            this.money = i;
        }

        public void getTeam(String disperse) {
            System.out.println("团队" + disperse);
        }
    }


    static class Customer {
        private String name;

        public Customer(String name) {
            this.name = name;
            System.out.println("my name is " + name);
        }

        public String getName() {
            return name;
        }

        public void say(String sayWhat) {
            System.out.println("客户" + name + "说：" + sayWhat);
        }

        public void refund() {
            System.out.println("客户" + name + "退款");
        }
    }

    static class Person {
        private String duty;
        private String name;
        private Person[] allPeople;
        private double share;

        public String getName() {
            return this.name;
        }

        public Person(String name, String duty) {
            this.name = name;
            this.duty = duty;
            System.out.println("my name is " + name + ",my duty is " + duty);
        }

        public void teamUp(Person... persons) {
            allPeople = persons;
            System.out.println(name + " team up these people,then talk about something");
        }


        public void say(String sayWhat) {
            System.out.println(name + "：" + sayWhat);
        }

        public void say(Person p, String sayWhat) {
            System.out.println(name + "对" + p.getName() + "说" + sayWhat);
        }


        public void getAllPeople(String sayWhat) {
            for (Person person : allPeople) {
                person.say(sayWhat);
            }
        }

        public void promise(Person b, String how, double v) {
            System.out.println(name + "承诺" + b.getName() + "完成" + how + "给" + v + "技术股");
            b.setShare(v);
        }


        public void setShare(double v) {
            this.share = v;
        }

        public double getShare() {
            return this.share;
        }

        public void development() {
            System.out.println(name + "研发");
        }

        public void developmentWith(Person c) {
            System.out.println(c.getName() + "在" + name + "的基础上继续研发");
        }

        public void getOrder(MyRealStory.Customer customer) {
            System.out.println(name + "接到客户" + customer.getName() + "的订单");
        }

        public void design() {
            System.out.println(name + "开始设计");
        }

        public void construction(Person c) {
            System.out.println(name + "以及" + c.getName() + "去现场施工");
        }

        public void ability(double v) {
            System.out.println(name + "能力值为:" + v);
        }

        public void addInvest(Person e) {
            System.out.println(name + "增加投资人:" + e.getName());
        }

        public void quarrel(Person a) {
            System.out.println(name + "和" + a.getName() + "吵架");
        }

        public void quit() {
            System.out.println(name + "退出");
        }

        public void quit(String reason) {
            System.out.println(name + "由于" + reason + "退出");
        }

        public void call(Person d, String back) {
            System.out.println(name + "叫" + d.getName() + back);
        }

        public void hateAndQuit(Person a) {
            System.out.println(name + "由于对" + a.getName() + "的恩怨也退出了");
        }
    }

}