/*
 * 编程语言：c++
 * 故事主题：两个和尚
 */
#include <iostream>
#include <string>
#include <windows.h>
using namespace std;
class Person{
	private:
		int age;
	public:
		void say(string str){
			cout<<str<<endl;
		}
		void setAge(int num){
			age = num;
		}
		int getAge(){
			return age;
		}
};
void sayStory() {
    cout<<"从前有个山，山里有个庙,"<<
            "庙里有个老和尚和小和尚,"<<
            "有一天，老和尚给小和尚讲故事,嘿，"<<
            "有人要用代码写故事，写的是什么故事呢？"<<
            "从前有个山，山里有个庙....."<<endl;
}
int main() {

    // 从前有个山，山里有个庙
    //庙里有个老和尚
    Person lhs;
    lhs.setAge(78);
    //和小和尚
    Person xhs;
    xhs.setAge(16);

    //有一天，小和尚说
    xhs.say("师傅师傅，给我讲个故事吧");
    //老和尚回答
    lhs.say("好的徒儿！故事要从为师看到的一条动弹说起，有人说要用代码写故事，写的是什么故事呢？");

    while (80 >= lhs.getAge()) {
        sayStory();
        Sleep(3600*24);

    }
    //老和尚不知不觉中BB了两年，在80岁的这一天去世了
    //小和尚悲痛欲绝，然后。。。。


    // 接
    // 小和尚一个人度过了60年
    xhs.setAge(78);

    // 这一天，庙里又来了一个小小和尚
    People xxhs;
    xxhs.setAge(16);

    // 有一天，小小和尚说
    xxhs.say("师傅师傅，给我讲个故事吧");
    xhs.say("好的徒儿！故事要从为师的师傅看到的一条动弹说起，有人说要用代码写故事，写的是什么故事呢？");
    
    while(80 >= xhs.getAge()){
        sayStory();
        Sleep(3600*24);
    }

    //小和尚不知不觉中BB了两年，在80岁的这一天去世了
    //小小和尚悲痛欲绝，然后。。。。

    // 续接
    // 小小和尚一个人度过了60年
    xxhs.setAge(78);

    // 这一天，庙里又来了一个小小小和尚
    People xxxhs;
    xxxhs.setAge(16);

    // 有一天，小小小和尚说
    xxxhs.say("师傅师傅，给我讲个故事吧");
    xxhs.say("好的徒儿！故事要从为师的师傅看到的一条动弹说起，有人说要用代码写故事，写的是什么故事呢？");
    xxxhs.say("师傅师傅，什么是动弹呢");
    xxhs.say("你别管，为师先给你讲这个故事");
    
    while(80 >= xxhs.getAge()){
        sayStory();
        Sleep(3600*24);
    }

    //小小和尚不知不觉中BB了两年，在80岁的这一天去世了
    //小小小和尚悲痛欲绝，嘴里喃喃道。。。。
    xxxhs.say("难道还要继续套娃吗");
    
    //然后。。。。


    
    
}