//编程语言：c++
//故事主题：枯燥老总爱上扫地玲 。。。

/*
 *
 *  玲旦愿   LingDanYuan
 *
 *  bilibili： 朱一旦的枯燥生活
 *
 */

#include <iostream>
#include <ostream>
#include <string>

using namespace std;

namespace LingDanYuan {                 // 玲旦愿

class Episode{         
public:
	Episode(int num) : Num(num) {}

	friend ostream &operator<<( ostream &output, const Episode &E )
      {
		  output << "=========="<<endl;
		  output << "  玲旦愿  "<<endl;
		  output << "  第"<< E.Num  <<"集"<<endl;
		  output << "=========="<<endl;

		  return output;
      }
protected:
	int Num;
};

class Tricycle{	
	friend ostream &operator<<( ostream &output, const Tricycle &T )
	{
		output << "+-----------------------+" << endl;
		output << "+   旦炒饭   烤冷面     +" << endl;
		output << "+-----------------------+" << endl;
		output << "+        三轮车         +" << endl;
		output << "+-----------------------+" << endl;
		output << "   o              o      " << endl;
        
        return output;
	}
};

class People{
	
public:
	People(string name, int gender) : Name(name), Gender(gender){}

protected:
	string Name;
	int Gender;    // 0 == 女, 1 == 男
};

class Actor : public People{
	public:
		Actor(string name, int gender, \
	       	  string charact,   \
			  string pet_phrase): People(name, gender),\
								  Charact(charact),    \
							      Pet_phrase(pet_phrase)
												  {}

	friend ostream &operator<<( ostream &output, const Actor &A )
	{
		output<<A.Name<<"出场"<<endl;

		return output;
	}
	void say(string str="")
	{
		cout<<Name<<": ";
		if(str == "")
		{
			cout<<Pet_phrase<<endl;
		}
		else
		{
			cout<<str<<endl;
		}
	}

	void think(string str)
	{
		cout<<Name<<": ";
		cout<<str<<endl;
	}

	void act(string act)
	{
		cout<<Name;
		cout<<act<<endl;
	}

	private:
		string Charact;
		string Pet_phrase;
};


// 男主
LingDanYuan::Actor Dan("朱一旦", 1, "劳力越带，责任越大(dai); 内八字", "有钱人的生活就是这么朴实无华且枯燥");
// 女主
LingDanYuan::Actor Ling("马小玲", 0, "", "嫩娘个腿");

}


void LingDanYuan01()
{
	using namespace LingDanYuan;
	

	//
	// 剧情太长，只挑了几个动作和对话
	//

	Episode episoe(1);

	cout<<episoe;

	Tricycle tricycle;
	cout<<tricycle;

	cout<<Dan;          // 旦总骑着三轮车出场

	Dan.say("每个月都会骑着我收购的三轮车，找个随遇而安的地方，卖烤冷面。");

	Actor SanLaizi("三赖子",1,"底层男孩", "");
	cout<<SanLaizi;

	SanLaizi.act("点了个底层套餐");

	Dan.act("(故意)加了个索马里进口海参");
	SanLaizi.act("假装没看见，给了5块钱");

	Dan.act("把烤冷面递给他，并露出劳力士");
	SanLaizi.act("颤抖，深深鞠躬");
	SanLaizi.act("回家后，把烤冷面珍藏起来");

	Actor SanLaiziSunzi("三赖子孙子" ,1, "底层男孩的孙子", "");
	cout<<SanLaiziSunzi;

	SanLaizi.say("年轻时，曾得到过一个戴劳力士的人卖的烤冷面");
	SanLaiziSunzi.act("以后打麻将也敢赢村子儿子了");

	cout<<std::endl;
	Dan.say();
	cout<<std::endl;

	cout<<Ling;
	Ling.say("老板,你这旦炒饭没有蛋也就算了，吃着吃着还吃出一块手表,硌的我牙生疼");
	
	Dan.think("是她了，终于遇见了一个不认识劳力士的女孩");
	Dan.say("你想怎麽办");

	Ling.say("怎么着也得给我50块钱");

	Dan.think("50块就能给她快乐，这样的知足的女孩，现在很难找到了");
	Dan.act("帮女孩去掉嘴边的饭渣");
	Dan.say("嫁给我吧");

	Ling.say("神经病啊，也不看看你是干啥的，赖蛤蟆想吃天鹅肉");

	Dan.think("人生中第一次被瞧不起，这感觉特别好。我一定要得到她");


	cout<<endl;

}	

void LingDanYuan02()
{
  using namespace LingDanYuan;
  
  //
  // 话接LinDanYuan01
  //

  Episode episode(2);

  cout << episode;

  Dan.say("或许这真的就是缘分，而你刚好就是那只天鹅");

  Ling.think("这人怕不是有毛病吧，要完钱得赶紧走！");
  Ling.act("退后并用手指着");
  Ling.say("别东扯西扯的，赶紧给钱！")

  Dan.act("轻轻地用手往下划，示意别着急");
  Dan.say("钱总会有的，但是一旦多了可就枯燥无味了");
  Dan.think("钱啊！～～～枯燥无味");
  Dan.say("何不如我娶你，一同体验这枯燥呢？");

  Ling.act("一脸惊愕！");
  Ling.think("怕不是神经病哟")
  Ling.say("我才不管你枯燥不枯燥，赶紧赔钱走人");

  Dan.act("从裤兜里拿出一张红钞！手上的劳力士在举止之间竟被太阳反射得熠熠生辉");

  cout << endl;
}


void LingDanYuan03()
{
	
	using namespace LingDanYuan;


    Episode episode(3);

	cout<<episode;


	// 场景
	cout<<"================="<<endl;
	cout<<"+  面试等待区   +"<<endl;
	cout<<"================="<<endl;

	// 出场
	cout<<Dan;
	Dan.think("哦，这不是我昨天伪装成普通人，体验底层生活时，遇见的那个女孩");
	cout<<Ling;
	Ling.act("等待面试中");
	
	Dan.think("她怎么来我的公司了，难道说这就是缘分");
	Ling.say("哟，这不是昨天那个癞蛤蟆吗?");

	Dan.think("癞蛤蟆，好别致的外号，没想到在她心里还惦记着我");
	Dan.say("你来干什么");

	Ling.say("你瞎啊");

	Dan.think("好犀利的反驳，我的心仿佛被戳中了");
	Ling.say("咋，你也来面试啊");

	Dan.think("好单纯的女孩，且不说我二十多万的劳力士，光是身上这件全球限量版的驴皮大衣，就能供他全家吃一年海参");

	// ...
	// 旦和玲 坐在办公室
	
	Dan.think("都三个小时过去了，她还没发现我是这家公司的老板");

	Ling.say("唉，癞蛤蟆，你也是来应聘保洁的吗？");
	Dan.think("原来是应聘保洁的，30不到就敢挑战50多才能胜任的工作，是个女强人");

	Ling.act("打了一下Dan的头");
	Ling.say("问你话呢，是不是?");

	Dan.think("敢打我的头，好奇怪的感觉，我越来越喜欢她了");
	Dan.say("是");

	Ling.say("我劝你回去吧，我干这行已经一万个小时了, 你吃不了这朝九晚五的饭，还是回去买你的旦炒饭吧");


	// 旦和玲开始比打扫卫生
	Actor JingLi("公司经理" ,1, "旦总公司经理; 玲的老公", "");
	cout<<JingLi;

	JingLi.say("啊，你怎么搞得");

	JingLi.act("跪了下来");
	
	cout<<endl;


}

void LingDanYuan04()
{
	using namespace LingDanYuan;
	
    Episode episode(4);

	cout<<episode;

	// 朱总办公室
	//
	
	Actor JingLi("公司经理" ,1, "旦总公司经理; 玲的老公", "");
	cout<<JingLi;

	cout<<Dan;
	cout<<Ling;
	cout<<JingLi;

	JingLi.say("啊，你这阿姨怎么搞得");

	Dan.think("哦，这样的话，我是老板的身份就要暴露了");

	// 旦总名场面
	//  hhhhhhhhhh
	Dan.say("八鹿户, 西莫迪，菠萝蜜，时光倒流");

	Ling.say("什么玩意");

	JingLi.act("跪");
	JingLi.say("我可不敢啊朱总");

	Dan.say("想想你在非洲的家人");

	// hhhh
	
	JingLi.act("坐在朱总座位上");
	JingLi.say("那你们两个都留下吧");

	Ling.say("啥意思，他也能留下");
	Ling.say("那我不干了");

	Ling.say("我那一摞获奖证书你不是没看，跟一个糙老爷们平起平坐，我忍不下去,再说了，你没看见他是个残疾");
	Ling.act("指着旦的内八");

	//
	JingLi.say("啊");
	JingLi.act("打了玲一个耳光");

	Dan.think("敢打我的女人");

	Ling.think("好有正义感的男人，只是因为我侮辱了残疾人，就当机立断的教育了我，好有男人味");
	JingLi.say("看我不打烂你的嘴");

	Dan.act("抓住了经理的手");
	Dan.think("她一定感受到了被保护的感觉");
	Dan.act("打了经理一个耳光");

	Ling.act("打了旦一个耳光");
	Ling.say("你凭什么打他");

	Dan.think("原来耳光是这种感觉,怪不得之前那些下人都喜欢被我打耳光");

	Ling.say("发什么呆，还不赶紧道歉");

	JingLi.say("啊");
	JingLi.act("打了玲一个耳光");

	Dan.think("果然她也喜欢被人打耳光");
	Dan.say("让我来");
	Dan.act("打了玲一个花式耳光");

	Ling.act("踢了旦一脚");

	Actor LingBa("保安" ,1, "旦总公司保安; 玲的老爸", "");
	cout<<LingBa;

	LingBa.say("啊");
	LingBa.act("晕了过去");
	

	cout<<endl;

	

}

void LingDanYuan05()
{
	using namespace LingDanYuan;
	
    Episode episode(5);
	cout<<episode;

	Actor LingBa("保安" ,1, "旦总公司保安; 玲的老爸", "");
	cout<<LingBa;


	Ling.say("爸，你怎么啦");
	Ling.say("老公，你快来看看");

	Dan.think("哦，这就是底层的爱情，怎么直接冲动，也好，我这就打给月子中心，预订房间");

	Actor JingLi("公司经理" ,1, "旦总公司经理; 玲的老公", "");
	cout<<JingLi;

	JingLi.say("爸，你怎么了");

	Dan.think("我喜欢的女人果然很抢手");


	// 回想
	// 玲爸看见了年轻人乱扔香蕉皮
	LingBa.say("阿西吧, 现在的年轻人越来越没有素质了");

	LingBa.act("拍完照，把香蕉皮从高楼扔下");

	// 玲爸给旦总送礼，想当保安科科长
	// 此时正好遇见旦总在时光倒流
	
	LingBa.act("没看香蕉皮，直接走进了旦总办公室");

	// 玲爸看见经理扶(抱)着旦总，以为在ghs
	//
	LingBa.say("啊，造孽啊");
	LingBa.act("晕了过去");

	Ling.say("快带咱爸去医院");
	JingLi.say("行，你等我去说一声");

	// 到办公室里，经理被叫去讲成功经验
	//
	
	Ling.say("急死个人了，快点啊");
	Dan.say("女人，要不要我帮你");

	Ling.say("滚开这里");

	Ling.act("抱起玲爸，走了出去");
	Ling.act("在楼道中，不小心踩到了刚刚的香蕉皮 ");

	LingBa.act("从高楼飞了出去");
	JingLi.act("刚刚讲完成功经验回来");
	

	cout<<endl;

}

void LingDanYuan06()
{

    using namespace LingDanYuan;
	
    Episode episode(6);
	cout<<episode;

    Actor LingBa("保安" ,1, "旦总公司保安; 玲的老爸", "");
	cout<<LingBa;

    Actor JingLi("公司经理" ,1, "旦总公司经理; 玲的老公", "");
	cout<<JingLi;

    Actor JingLiXiaoSan("公司经理小三", 0, "公司经理的小三", "");
    cout<<JingLiXiaoSan;

    JingLiXiaoSan.act("推着孩子在街边散步，听见上面好像有东西掉下来");
    JingLiXiaoSan.act("赶紧去保护孩子");

    Actor JiuRou("酒肉朋友", 1, "旦总酒肉朋友", "瞪眼");
    cout<<JiuRou;
    
    JiuRou.act("看见高楼上玲爸飞了下来");
    JiuRou.act("发动技能：瞪眼");
    JiuRou.act("一道激光，把玲爸射爆炸了");

    Ling.act("跪");
    Ling.say("爸");

    JingLi.act("跪");
    JingLi.say("岳父");

    Dan.think("说实话，我有点嫉妒他。平凡了一辈子，临走时打破了一项世界记录。从想当保安科长到被火花完毕用了不到一小时。那么问题来了，谁应该对他死负责");
    
    Ling.say("我们离婚吧");
    JingLi.say("啊，为什么");
    Ling.say("你先解释一下，她是谁");
    Ling.say("且不说你们一家三口的合影，光看这孩子的长相，我也该明白了");
    JingLi.say("小玲，你听我解释啊，我这也是为了你啊");
    Ling.say("为了嫩娘了腿");

    Ling.say("我本以为，洗洁精搓出的泡沫，是世界上最脆弱的东西，结了婚才发现，婚姻更脆弱");
    
    JingLi.say("行，那我们离婚吧");

    Dan.say("我们结婚吧");
    Ling.say("癞蛤蟆，强扭的瓜不甜，你这样趁虚而入，只能得到我一时的肉体，得不到我永生的灵魂");

    Dan.think("我爷爷没有输给战火, 我爸爸没有输给贫穷，而我却在和平的年代沦陷给了保洁阿姨");

    Dan.act("旦抱住了晕倒的玲");

    cout<<endl;

}



     
int main()
{
	LingDanYuan01();

	LingDanYuan02();

    LingDanYuan03();

    LingDanYuan04();

    LingDanYuan05();

    LingDanYuan06();
    
	// ...
	//
	
	return 0;

}