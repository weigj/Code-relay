//编程语言： js
//故事主题：猴子捞月
class monkey {
    constructor(year, postion, time) {
        this.year = year;
        this.do = "";
        this.look = "";
        this.postion = postion;
        this.time = time
    }
    doing(thing) {
        this.do = thing;
    }
    lookAt(where) {
        this.look = where;
    }
    go(postion) {
        this.postion = postion;
    }
    say(str) {
        console.log(str);
    }

}
//一天傍晚，几只小猴子来到井边,树上坐着一只老猴子。
let smallMonkey1 = new monkey(5, "井边" , 17);
let smallMonkey2 = new monkey(4, "井边" , 17);
let smallMonkey3 = new monkey(6, "井边" , 17);
let oldMonkey = new monkey(70, "树上", 17);
//一只小猴子看见了井里月亮的倒影
let falseMoon = new Object({type:"倒影"})
let trueMoon = new Object({type: "真实"})
smallMonkey1.lookAt(falseMoon);
//它马上叫了起来：“月亮掉进井里了！月亮掉进井里了！”
smallMonkey1.say("月亮掉进井里了！月亮掉进井里了！");
//其他几只小猴子也看见了，决定要把月亮捞出来
smallMonkey2.lookAt(falseMoon);
smallMonkey3.lookAt(falseMoon);
smallMonkey2.say("咱们得把月亮捞出来。");
//于是他们到大树上去一个接一个的倒挂下来
smallMonkey1.go("大树");
smallMonkey2.go("大树");
smallMonkey3.go("大树");
smallMonkey1.doing("脚挂在大树上，手抓住第二只小猴子的腿");
smallMonkey2.doing("脚被第一只小猴子抓住，手抓住第三只小猴子的腿");
smallMonkey3.doing("脚被第二只小猴子抓住，手去捞月亮");
//第三只小猴子的手刚碰到水面，水面上的月亮就没了
smallMonkey3.doing("用手去够倒影");
delete moon;
//这时，老猴子说：“天上的月亮还在好好的挂着呢。”，几只小猴子一看，果然如此。
oldMonkey.say("天上的月亮还在好好的挂着呢。");
smallMonkey1.lookAt(trueMoon);
smallMonkey2.lookAt(trueMoon);
smallMonkey3.lookAt(trueMoon);