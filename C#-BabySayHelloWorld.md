using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyFamily
{
    [DateOfBirth("2016-12-23 15:00:12")]
    public class Baby : Father, IMother
    {
        public string SayHelloWorld()
        {
            return SayHelloWorld(Environment.NewLine);
        }

        public string SayHelloWorld(string newLine)
        {
            Dictionary<string, string> dic_Info = new Dictionary<string, string>
            {
                {"姓名", SurName+GetPersonal_Name()},
                {"性别", GetGender(Gender.X)},
                {"生日", GetDatOfBirth()},
                {"体重", "7.6斤"},
                {"介绍", string.Format(@"小宝继承了父亲{0},并有着母亲{1},{2}。
                                                    来到这世界上说了句HelloWorld是<{3}>",
                    GetFeature(), GetSkin(),GetCharacter(),GetSayHello()  )},
            };

            StringBuilder sb_info = new StringBuilder();
            foreach (KeyValuePair<string, string> t in dic_Info)
            {
                sb_info.AppendFormat("{0}:{1}{2}", t.Key, t.Value, newLine);
            }

            return sb_info.ToString();
        }


        public override string GetPersonal_Name()
        {
            //淡泊明志 宁静致远
            return "泊远";
        }

        protected static string GetGender(Gender gender)
        {
            Type type = gender.GetType();
            var fd = type.GetField(gender.ToString());
            if (fd == null)
                return string.Empty;
            object[] attrs = fd.GetCustomAttributes(typeof(GenderAttribute), false);
            string name = string.Empty;
            foreach (GenderAttribute attr in attrs)
            {
                name = attr.GenderText;
            }
            return name;
        }

        private static string GetDatOfBirth()
        {
            var type = typeof(Baby);
            var attribute = type.GetCustomAttributes(typeof(DateOfBirth), false).FirstOrDefault();
            return ((DateOfBirth) attribute)?.Date.ToString("yyyy年MM月dd日");
        }

        public string GetCharacter()
        {
            return "温雅的性格";
        }

        public string GetSkin()
        {
            return "白色的皮肤";
        }

        private object GetSayHello()
        {
            return "吭叽";
        }
    }

    public class Father
    {
        public string SurName { get; set; } = "Z";
        public virtual string GetPersonal_Name()
        {
            return "xx";
        }

        public string GetFeature()
        {
            return "帅气容貌";
        }
    }

    public interface IMother
    {
        string GetSkin();

        string GetCharacter();
    }

    public class DateOfBirth : Attribute
    {
        private DateTime _date;
        public DateTime Date
        {
            get { return _date; }
        }
        public DateOfBirth(string date)
        {
            _date = DateTime.Parse(date);
        }
    }

    public enum Gender
    {
        [Gender("男")]
        X,
        [Gender("女")]
        Y
    }

    public class GenderAttribute : Attribute
    {
        public string GenderText { get; set; }
        public GenderAttribute(string genderText)
        {
            this.GenderText = genderText;
        }
    }

}