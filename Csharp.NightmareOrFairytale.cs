using System;

namespace NightmareFairytale
{
    /// <summary>心情状态</summary>
    public enum EmotionState
    {
        Normal,
        Happy,
        MoreHappy,
        Sad,
        MoreSad
    }
    /// <summary>人生状态</summary>
    public enum LifeState
    {
        Nice,
        Poor,
    }

    public interface ILove
    {
        bool Meet(Person person);
        bool FallInLove(Person person);
        bool GetMarried(Person person);
        bool GetParted(Person person);
    }

    public interface IHabit
    {
        void GoHike();
        void HaveDinner(Person person);
        void HaveADate(Person person);
    }

    public interface IState
    {
        EmotionState Emotion { get; set; }
        LifeState Life { get; set; }
    }

    public abstract class Person : IState
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public virtual EmotionState Emotion { get; set; }
        public virtual LifeState Life { get; set; }
    }
    public class Male : Person
    {

    }
    public class Female : Person
    {

    }

    public class Kinght : Male,  ILove,IHabit
    {
        public Kinght()
        {

        }
        public Kinght(string name)
        {
            this.Name=name;
        }

        public Kinght(string name,int age)
        {
            this.Name=name;
            this.Age=age;
        }
        public override EmotionState Emotion
        {
            get
            {
                return base.Emotion;
            }
            set
            {
                base.Emotion = value;
                switch (base.Emotion)
                {
                    case EmotionState.Normal:
                        Console.WriteLine("{0}心情一般。",Name);
                        break;
                    case EmotionState.Happy:
                        Console.WriteLine("{0}心情好！",Name);
                        break;
                    case EmotionState.MoreHappy:
                        Console.WriteLine("{0}心情非常好！",Name);
                        break;
                    case EmotionState.Sad:
                        Console.WriteLine("{0}心情很差！",Name);
                        break;
                    case EmotionState.MoreSad:
                        Console.WriteLine("{0}心情极糟！",Name);
                        break;
                    default:
                        break;
                }
            }
        }

        public bool Meet(Person person)
        {
            Console.WriteLine("偶遇{0}",person.Name);
            this.Emotion=EmotionState.Happy;
            return true;
        }

        public bool FallInLove(Person person)
        {
            Console.WriteLine("{0}与{1}陷入爱河",this.Name,person.Name);
            this.Emotion=EmotionState.MoreHappy;
            return true;
        }

        public bool GetMarried(Person person)
        {
            Console.WriteLine("{0},{1}两人结婚了！",this.Name,person.Name);
            this.Emotion=EmotionState.MoreHappy;
            return true;
        }

        public bool GetParted(Person person)
        {
            Console.WriteLine("{0},{1}两人分手了！",this.Name,person.Name);
            this.Emotion=EmotionState.MoreSad;
            return true;
        }

        public void GoHike()
        {
            Console.WriteLine("{0}去徒步了。。。",this.Name);
            this.Emotion=EmotionState.Sad;
        }

        public void HaveDinner(Person person)
        {
            Console.WriteLine("{0}与{1}两人共进晚餐。",this.Name,person.Name);
            this.Emotion=EmotionState.Happy;
        }

        public void HaveADate(Person person)
        {
            Console.WriteLine("{0},{1}约会",this.Name,person.Name);
            this.Emotion=EmotionState.Happy;
        }
    }
    public class Princess : Female
    {
        public Princess()
        {

        }
        public Princess(string name)
        {
            this.Name=name;
        }
    }
    public class Knightness : Female
    {
        public Knightness()
        {

        }
        public Knightness(string name)
        {
            this.Name=name;
        }
    }
    public class Queen : Female
    {

    }

    public class King:Male
    {

    }
    public class MyLife
    {
        static void Main(string[] args)
        {

            Kinght knight = new Kinght("1");
            Princess princess=new Princess("0");
            Knightness knightness=new Knightness("2");

            knight.Meet(princess);
            knight.Meet(princess);
            knight.Meet(princess);

            knight.HaveDinner(princess);
            knight.HaveADate(princess);
            
            knight.GoHike();
            knight.Meet(knightness);

            knight.HaveDinner(princess);
            
            knight.FallInLove(princess);

            knight.GetMarried(princess);
            

            
        }

    }
}
